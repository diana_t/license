angular.module('license', ['ui.bootstrap', 'ngAnimate', 'facebook', 'ngSanitize'])
    .config(['$httpProvider', function($httpProvider, API_URL) {
        $httpProvider.interceptors.push('authInterceptor');
    }])
    .config(function(FacebookProvider) {
        FacebookProvider.init('1914854608758564');
    })
    .constant('API_URL', 'https:localhost:3000/');