'use strict';
angular.module('license')
    .factory('authToken', function($window) {
        var storage = $window.localStorage;
        var authKeys = ['token', 'name'];
        var userInfo = {
            'token': '',
            'name': ''
        };

        var authService = {
            setToken: function(token) {
                userInfo.token = token;
                storage.setItem(authKeys[0], token);
            },

            getToken: function() {
                if (!userInfo.token) {
                    userInfo.token = storage.getItem(authKeys[0]);
                }
                return userInfo.token;
            },

            setName: function(name) {
                userInfo.name = name;
                storage.setItem(authKeys[1], name);
            },

            getName: function() {
                if (!userInfo.name) {
                    userInfo.name = storage.getItem(authKeys[1]);
                }
                return userInfo.name;
            },

            isAuthenticated: function() {
                return !!authService.getToken();
            },

            removeUser: function() {
                userInfo = null;
                storage.removeItem(authKeys[0]);
                storage.removeItem(authKeys[1]);
            },
        };

        return authService;
    })
    .factory('authInterceptor', function(authToken) {
        return {
            request: function(config) {
                var token = authToken.getToken();
                if (token) {
                    config.headers.Authorization = 'Bearer ' + token;
                }
                return config;
            },
            response: function(response) {
                return response;
            }
        };
    })
    .service('authService', function authService(authToken, $window, $http) {

        this.localLogin = function(user) {
            var url = '/login/submit';
            return $http.post(url, user)
                .success(function(result) {
                    if (result.token && result.name) {
                        authToken.setToken(result.token);
                        authToken.setName(result.name);
                    }
                    return result;
                })
                .error(function(error) {
                    return error;
                });
        };

        this.googleAuth = function() {

            var OAUTHURL = 'https://accounts.google.com/o/oauth2/auth?';
            var SCOPE = 'email profile';
            var CLIENTID = '232257423981-ib6248htl5m2jl0dvfd2m9kphudbmfbc.apps.googleusercontent.com';
            var REDIRECT = 'http://localhost:8080/';
            var TYPE = 'token';

            var urlBuilder = [];
            urlBuilder.push('response_type=code',
                'client_id=' + CLIENTID,
                'redirect_uri=' + window.location.origin,
                'scope=' + SCOPE);


            var _url = OAUTHURL + urlBuilder.join('&');
            var options = 'width=500,height=500,left=' + ($window.outerWidth - 500) / 2 + ',top=' + 100;

            var win = $window.open(_url, 'winGoogleAuth', options);
            $window.focus();

            var pollTimer = $window.setInterval(function() {
                try {
                    if (win.document.URL.indexOf(REDIRECT) !== -1) {
                        window.clearInterval(pollTimer);
                        var pair = win.document.URL.split('=');
                        var code = decodeURIComponent(pair[1]);
                        var params = {
                            code: code,
                            redirectUri: window.location.origin,
                            clientId: CLIENTID
                        };
                        sendGoogleAuth(params);
                        win.close();
                    }
                } catch (e) {}
            }, 100);
        };

        this.sendFacebookAuth = function(response) {
            var url = '/login/facebook';
            $http.post(url, response)
                .then(function(result) {

                    if (result.data.token && result.data.name) {
                        location = '/account';
                        authToken.setToken(result.data.token);
                        authToken.setName(result.data.name);
                    }

                })
                .catch(function(error) {
                    var location = '/error/401';
                });
        };

        this.logout = function() {
            location = '/';
            authToken.removeUser();
        };

        function sendGoogleAuth(params) {
            var url = '/login/google';
            $http.post(url, params)
                .then(function(result) {

                    if (result.data.token && result.data.name) {
                        location = '/account';
                        authToken.setToken(result.data.token);
                        authToken.setName(result.data.name);
                    }

                })
                .catch(function(error) {
                    var location = '/error/401';
                });
        }

        this.validateEmail = function(emailString) {
            var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return regex.test(emailString);
        };

    });