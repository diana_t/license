'use strict';

angular.module('license')
    .controller('mainController', ['authToken', '$scope', 'API_URL', '$window',
        function(authToken, $scope, API_URL, $window) {
            $scope.isAuthenticated = authToken.isAuthenticated();
            $scope.name = authToken.getName();
        }
    ]);