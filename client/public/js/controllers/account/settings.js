'use strict';

angular.module('license')
    .controller('settingController', ['authToken', '$scope', '$http',
        function(authToken, $scope, $http) {

            $scope.isAuthenticated = authToken.isAuthenticated();
            $scope.name = authToken.getName();

            $scope.getSettings = function() {
                var url = '/account/api/settings';
                $http.get(url)
                    .then(function(success) {
                        return success;
                    })
                    .catch(function(error) {
                        return {};
                    });
            };
        }
    ]);