'use strict';

angular.module('license')
    .controller('homeController', ['authToken', '$scope', '$http',
        function(authToken, $scope, $http) {
            $scope.isAuthenticated = authToken.isAuthenticated();
            $scope.name = authToken.getName();
        }
    ]);