'use strict';

angular.module('license')
    .controller('loginController', ['$scope', '$location', '$http', 'authToken', '$sce', 'authService', 'Facebook',
        function($scope, $location, $http, authToken, $sce, authService, Facebook, ngSanitize) {

            $scope.validEmail = true;
            $scope.validLogin = true;
            $scope.showAlert = '';

            $scope.localLogin = function() {
                if (!authService.validateEmail($scope.user.email)) {
                    $scope.validEmail = false;
                }
                if (authService.validateEmail($scope.user.email)) {
                    $scope.validEmail = true;
                }
                if ($scope.user.password && $scope.validEmail === true) {
                    var url = '/login/submit';
                    authService.localLogin($scope.user)
                        .success(function(result) {

                            location = '/account';

                        })
                        .error(function(err) {
                            if (err === 'Unauthorized') {
                                var url = '/components/alert';
                                $scope.validLogin = false;
                                $http.post(url, { 'code': 'LOGIN_UNAUTHORUZED' })
                                    .then(function(response) {
                                        $scope.showAlert = $sce.trustAsHtml(response.data);
                                    });
                            }
                        });
                }
            };

            $scope.googleLogin = function() {
                authService.googleAuth();
            };

            $scope.facebookLogin = function() {
                Facebook.login(function(response) {
                    if (response.status === 'connected') {
                        authService.sendFacebookAuth(response);
                    }
                }, { 'scope': 'email,user_photos' });
            };

        }
    ]);