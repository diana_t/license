'use strict';

angular.module('license')
    .controller('registerController', ['$scope', '$location', '$http', 'authToken', '$sce', 'authService',
        function($scope, $location, $http, authToken, $sce, authService) {

            $scope.validEmail = true;
            $scope.isPasswordStrong = false;
            $scope.isMediumStrong = false;
            $scope.errorCode = 0;
            $scope.showAlert = '';

            $scope.Submit = function() {

                if (!authService.validateEmail($scope.register.email)) {
                    $scope.validEmail = false;
                    return;
                }
                if (authService.validateEmail($scope.register.email)) {
                    $scope.validEmail = true;
                }
                if ($scope.strongPassword === 3 || $scope.strongPassword === 2) {
                    $scope.isMediumStrong = true;
                    return;
                }
                var completeForm = !$scope.passwordMatch && !$scope.register.firstname && !$scope.register.lastname;
                if (!completeForm) {
                    var url = '/register/submit';
                    $http.post(url, $scope.register)
                        .then(function(success) {
                            location = '/account';
                            authToken.setToken(success.data.token);
                            authToken.setName(success.data.name);
                        })
                        .catch(function(error) {
                            if (error.data === 'Unauthorized') {

                                var url = '/components/alert';
                                $scope.validLogin = false;

                                $http.post(url, { 'code': 'REGISTRATION_GENERAL_ERROR' })
                                    .then(function(response) {
                                        $scope.showAlert = $sce.trustAsHtml(response.data);
                                    });
                            }
                        });
                }
            };

            var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
            var mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");

            $scope.analyze = function(value) {
                if (strongRegex.test(value)) {
                    $scope.strongPassword = 1;
                    $scope.isPasswordStrong = false;
                } else if (mediumRegex.test(value)) {
                    $scope.strongPassword = 2;
                    $scope.isPasswordStrong = true;
                } else {
                    $scope.strongPassword = 3;
                    $scope.isPasswordStrong = true;
                }
            };
            $scope.$watch('register.password', function(value) {
                if (!_.isUndefined($scope.register.passwordConfirm)) {
                    $scope.passwordMatch = $scope.register.passwordConfirm === value ? 1 : 2;
                } else {
                    $scope.passwordMatch = 0;
                }

            });
            $scope.$watch('register.passwordConfirm', function(value) {
                if (!_.isUndefined($scope.register.password)) {
                    $scope.passwordMatch = $scope.register.password === value ? 1 : 2;
                } else {
                    $scope.passwordMatch = 0;
                }

            });

            $scope.getMatchPasswordClass = function(value, status) {
                switch (status) {
                    case 1:
                        return value + 'success';
                    case 2:
                        return value + 'warning';
                    case 3:
                        return value + 'danger';
                    default:
                        return '';
                }
            };
        }
    ]);