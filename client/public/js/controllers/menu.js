'use strict';

angular.module('license')
    .controller('menuController', ['authToken', '$scope', '$http', '$uibModal', '$log', '$sce', '$document', 'authService',
        function(authToken, $scope, $http, $uibModal, $log, $sce, $document, authService) {

            $scope.isAuthenticated = authToken.isAuthenticated();
            $scope.name = authToken.getName();

            $scope.login = function() {
                $http.get('/login')
                    .then(function(result) {

                        var modalInstance = openModal(result.data);

                        modalInstance.result.then(function(selectedItem) {
                            $scope.selected = selectedItem;
                        }, function() {
                            $log.info('Modal dismissed at: ' + new Date());
                        });
                    });
            };

            $scope.logout = function() {
                authService.logout();
            };

            function openModal(htmlTemplate) {
                var ModalInstanceCtrl = function($scope, $uibModalInstance, userForm) {
                    $scope.cancel = function() {
                        $uibModalInstance.dismiss('cancel');
                    };
                };
                var myModal = $uibModal.open({
                    template: htmlTemplate,
                    controller: ModalInstanceCtrl,
                    scope: $scope,
                    size: 'lg',
                    resolve: {
                        userForm: function() {
                            return $scope.userForm;
                        }
                    }
                });
                return myModal;
            }

        }
    ]);