var express = require('express');
var router = express.Router();
var moment = require('moment');
var _ = require('lodash');
var utils = require('../services/utils.js')();
var dbCommands = require('../services/dbCommands.js');
var jwt = require('jwt-simple');
var Promise = require('bluebird');

var emailServices = require('../services/emailService.js');

module.exports = function(db) {

    //Routes
    router.get('/', function(req, res) {
        res.render('account/index');
    });

    router.get('/settings', function(req, res) {
        res.render('account/settings');
    });

    router.get('/logout', function(req, res) {
        res.clearCookie('session');
        res.clearCookie('username');
        res.clearCookie('fr');
        res.render('main/index');
    });

    //API - Account Service

    router.get('/api/settings', function(req, res) {
        var token = req.headers.Authorization.split(' ')[1];
        var payload = jwt.decode(token, 'shhh...');
        if (!payload.sub) {
            res.status(401).send({
                message: 'Authentification failed'
            });
        }
        res.send(token);
    });

    router.get('/verifyEmail', function(req, res) {
        emailServices.handler(req, res, db);
    });

    return router;

};