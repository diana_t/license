var express = require('express');
var router = express.Router();
var utils = require('../services/utils.js')();
var alert = require('../config/alerts.json');
var dbCommands = require('../services/dbCommands.js');
var fbService = require('../services/facebookService.js')();
var googleService = require('../services/googleService.js')();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var request = require('request');
var _ = require('lodash');
var moment = require('moment');
var emailServices = require('../services/emailService.js');

module.exports = function(db) {
    //Passport Strategy 
    var strategyLogin = new LocalStrategy({ usernameField: 'email' }, function(email, password, done) {
        var findJson = {
            'email': email
        };
        dbCommands.findOne(db, 'users', findJson).then(function(user, err) {
            if (err) {
                return done(null, false, 'LOGIN_GENERAL_ERROR');
            }
            if (!user) {
                return done(null, false, 'LOGIN_USER_NOT_FIND');
            }
            utils.comparePasswords(user.password, password, function(err, isMatch) {
                if (err || !isMatch) {
                    return done(null, false, 'LOGIN_WRONG_EMAIL_PASS');
                }
                return done(null, user);
            });

        });
    });
    passport.serializeUser(function(user, done) {
        done(null, user);
    });
    passport.deserializeUser(function(user, done) {
        done(null, user);
    });
    passport.use('local-login', strategyLogin);
    //Routes
    router.get('/', function(req, res) {
        // emailServices.send('turnea.diana@gmail.com');
        res.render('login/index');
    });
    // Local Login
    router.post('/submit', passport.authenticate('local-login'), function(req, res) {
        emailServices.send('turnea.diana@gmail.com');
        utils.createToken(res, req.user);
    });
    // Google Login
    router.post('/google', function(req, res) {
        googleService.googleAuth(req, res, db);
    });
    //Facebook Login
    router.post('/facebook', function(req, res) {
        fbService.facebookAuth(req, res, db);
    });

    return router;
};