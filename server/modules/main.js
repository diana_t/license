var express = require('express');
var router = express.Router();
var errorHandler = require('../config/errorHandler.json');

module.exports = function() {

    router.get('/', function(req, res) {
        res.render('main/index', {
            title: 'Main Page'
        });
    });

    // router.get('/:authCode', function(req, res) {
    //     console.log(req.params.authCode);
    //     res.render('main/index', {
    //         title: 'Main Page'
    //     });
    // });

    router.get('/header', function(req, res) {
        res.render('header/header', {
            title: 'Title'
        });
    });

    router.get('/menu', function(req, res) {
        res.render('header/menu', {
            title: 'Main Page'
        });
    });

    router.get('/footer', function(req, res) {
        res.render('header/footer', {
            title: 'Title'
        });
    });

    router.get('/error/:code', function(req, res) {
        res.render('error/index', {
            title: errorHandler[req.params.code].title,
            message: errorHandler[req.params.code].message
        });
    });

    return router;
};