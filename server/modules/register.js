var express = require('express');
var router = express.Router();
var moment = require('moment');
var _ = require('lodash');
var jwt = require('jwt-simple');
var Promise = require('bluebird');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var utils = require('../services/utils.js')();
var dbCommands = require('../services/dbCommands.js');
var emailServices = require('../services/emailService.js');

module.exports = function(db) {
    //Passport Strategy
    var strategy = new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    }, function(req, email, password, done) {
        var findFiels = {
            'email': email
        };
        dbCommands.findOne(db, 'users', findFiels).then(function(result) {
            if (result) {
                return done(null, false, 'REGISTRATION_USER_EXISTS');
            }
            var newUser = {
                'firstname': _.trim(req.body.firstname),
                'lastname': _.trim(req.body.lastname),
                'email': _.trim(email),
                'password': utils.passwordHash(_.trim(password)),
                'createAt': moment()
            };

            dbCommands.insert(db, 'users', newUser).then(function(result) {
                if (!_.isUndefined(result.insertedIds[0])) {
                    newUser._id = result.insertedIds[0];
                    return done(null, newUser);
                }
            }).catch(function(err) {
                if (err.code === 11000) {
                    return done(null, false, 'REGISTRATION_USER_EXISTS');
                }
                return done(null, false, 'REGISTRATION_GENERAL_ERROR');
            });

        }).catch(function(err) {
            return done(null, false, 'REGISTRATION_GENERAL_ERROR');
        });
    });
    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    passport.deserializeUser(function(user, done) {
        done(null, user);
    });
    passport.use('local-register', strategy);

    //Routes
    router.get('/', function(req, res) {

        res.render('register/index', {
            title: 'Register Page'
        });
    });

    //API
    router.post('/submit', passport.authenticate('local-register'), function(req, res) {
        emailServices.send(req.body.email);
        utils.createToken(res, req.user);
    });



    return router;

};