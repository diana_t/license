var express = require('express');
var router = express.Router();
var alert = require('../config/alerts.json');

module.exports = function() {

    router.post('/alert', function(req, res) {
        res.render('components/alert', alert[req.body.code]);
    });

    router.get('/modal', function(req, res) {
        res.render('components/modal');
    });

    return router;
};