var Promise = require('bluebird');

exports.insert = function(db, collection, obj) {
    return new Promise(function(resolve, reject) {
        db.collection(collection).insert(obj,
            function(err, results) {
                if (err) {
                    return reject(err);
                }
                return resolve(results);
            }
        );
    });
};

exports.findOne = function(db, collection, fields) {
    return new Promise(function(resolve, reject) {
        db.collection(collection).findOne(fields,
            function(err, results) {
                //console.info(results);
                if (err) {
                    return reject(err);
                }
                return resolve(results);
            }
        );
    });
};

exports.update = function(db, collection, searchItems, updateObj) {
    return new Promise(function(resolve, reject) {
        db.collection(collection).update(searchItems, updateObj,
            function(err, results) {
                console.info(results);
                if (err) {
                    return reject(err);
                }
                return resolve(results);
            }
        );
    });
};