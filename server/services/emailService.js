var config = require('../config/auth.js');
var emailTemplate = require('../config/emailTemplates.js');
var errorHandler = require('../config/errorHandler.json');
var jwt = require('jwt-simple');
var fs = require('fs');
var _ = require('underscore');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var dbCommands = require('../services/dbCommands.js');

var model = {
    verifyUrl: 'http://localhost:8080/account/verifyEmail?token=',
    title: 'Be there for your family',
    subTitle: 'Thanks for singing up!',
    body: 'Please verifu your email address by clicking the button below'
};

exports.send = function(email) {
    var payload = {
        sub: email
    };
    var token = jwt.encode(payload, config.EMAIL_SECRET);

    var transporter = nodemailer.createTransport(smtpTransport({
        service: 'Gmail',
        secure: true,
        auth: {
            user: 'license.diana.turnea@gmail.com',
            pass: 'Zambeste21!#'
        }
    }));

    var mailOptions = {
        from: 'Be there <license.diana.turnea@gmail.com>',
        to: email,
        subject: '[license] Account Verification',
        html: getHtml(token)
    };

    transporter.sendMail(mailOptions, function(err, info) {
        if (err) {
            return err;
        }
        console.log('email send');
    });
};

exports.handler = function(req, res, db) {
    var token = req.query.token;
    var payload = jwt.decode(token, config.EMAIL_SECRET);
    var email = payload.sub;

    if (!email) {
        return handleError(res, 'EMAIL_TOKEN_INVALID');
    }
    var searchUser = {
        'email': email
    };
    dbCommands.findOne(db, 'users', searchUser).then(function(foundUser, error) {
        if (!foundUser) {
            return handleError(res, 'USER_NOT_FOUND');
        }
        if (foundUser.active === true) {
            return handleError(res, 'ACCOUNT_ACTIVE');
        }

        foundUser.active = true;
        dbCommands.update(db, 'users', { _id: foundUser._id },
            foundUser).then(function(result) {
            if (result) {
                return res.redirect(config.APP_URL);
            }
        }).catch(function(err) {
            return handleError(res, 'USER_NOT_FOUND');
        });

    });

};

function getHtml(token) {
    // var path = process.cwd();
    // var buffer = fs.readFileSync(path + '/server/views/emailVerification.html', encoding = 'utf8');

    var template = _.template(emailTemplate.verifyEmail);

    model.verifyUrl += token;
    return template(model);
}

function handleError(res, code) {
    res.status(401).send({
        title: errorHandler[code].title,
        message: errorHandler[code].message
    });
}

_.templateSettings = {
    interpolate: /\{\{(.+?)\}\}/g
};