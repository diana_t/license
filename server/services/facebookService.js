var config = require('../config/auth.js');
var request = require('request');
var qs = require('querystring');
var dbCommands = require('../services/dbCommands.js');
var utils = require('../services/utils.js')();
var _ = require('lodash');
var moment = require('moment');

module.exports = function() {
    function facebookAuth(req, res, db) {
        var graphApiUrl = 'https://graph.facebook.com/me?access_token=';
        var findUser = {
            'facebookId': req.body.authResponse.userID
        };
        dbCommands.findOne(db, 'users', findUser).then(function(foundUser, err) {
            if (foundUser) {
                return utils.createToken(res, foundUser);
            }
            var getProfileUrl = graphApiUrl + req.body.authResponse.accessToken + '&fields=first_name,last_name';
            request.get(getProfileUrl, function(err, response, profile) {
                var profileJson = JSON.parse(profile);
                console.log(profileJson);
                var newUser = {
                    'firstname': _.trim(profileJson['first_name']),
                    'lastname': _.trim(profileJson['last_name']),
                    'facebookId': profileJson['id'],
                    'createAt': moment()
                };
                dbCommands.insert(db, 'users', newUser).then(function(result) {
                    if (!_.isUndefined(result.insertedIds[0])) {
                        result._id = result.insertedIds[0];
                        return utils.createToken(res, newUser);
                    }
                }).catch(function(err) {
                    return res.send({ error: true, code: 'REGISTRATION_GENERAL_ERROR' });
                });
            });

        }).catch(function(err) {
            return res.send({ error: true, code: 'REGISTRATION_USER_EXISTS' });
        });
    }
    return {
        facebookAuth: facebookAuth
    };
};