var config = require('../config/auth.js');
var request = require('request');
var qs = require('querystring');
var dbCommands = require('../services/dbCommands.js');
var utils = require('../services/utils.js')();
var _ = require('lodash');
var moment = require('moment');

module.exports = function() {
    function googleAuth(req, res, db) {
        var url = 'https://accounts.google.com/o/oauth2/token';
        var apiUrl = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect';
        var params = {
            'client_id': req.body.clientId,
            'redirect_uri': req.body.redirectUri,
            'code': req.body.code,
            'grant_type': 'authorization_code',
            'client_secret': 'mwrEoLjw6pb0FaGwUgb9--Sp'
        };
        request.post(url, {
            json: true,
            form: params
        }, function(err, response, token) {
            var headers = {
                authorization: token['token_type'] + ' ' + token['access_token']
            };
            request.get({
                url: apiUrl,
                headers: headers,
                json: true
            }, function(err, response, profile) {
                var findUser = {
                    'googleId': profile.sub
                };
                dbCommands.findOne(db, 'users', findUser).then(function(foundUser, err) {
                    if (foundUser) {
                        return utils.createToken(res, foundUser);
                    }
                    var newUser = {
                        'firstname': _.trim(profile['given_name']),
                        'lastname': _.trim(profile['family_name']),
                        'googleId': profile['sub'],
                        'createAt': moment()
                    };
                    dbCommands.insert(db, 'users', newUser).then(function(result) {
                        if (!_.isUndefined(result.insertedIds[0])) {
                            result._id = result.insertedIds[0];
                            return utils.createToken(res, newUser);
                        }
                    }).catch(function(err) {
                        return res.send({ error: true, code: 'REGISTRATION_GENERAL_ERROR' });
                    });


                }).catch(function(err) {
                    return res.send({ error: true, code: 'REGISTRATION_USER_EXISTS' });
                });
            });
        });
    }
    return {
        googleAuth: googleAuth
    };
};