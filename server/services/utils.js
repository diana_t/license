var bcrypt = require('bcrypt-nodejs');
var jwt = require('jwt-simple');
var Promise = require('bluebird');

module.exports = function(db) {
    function passwordHash(password) {
        console.log(password);
        return bcrypt.hashSync(password);
    }

    function comparePasswords(hash, password, callback) {
        bcrypt.compare(password, hash, function(err, bool) {
            if (err) {
                return callback(err);
            }
            return callback(null, bool);
        });
    }

    function createToken(res, user) {
        var payload = {
            sub: user._id.toString()
        };

        var token = jwt.encode(payload, 'shhh...');

        return res.status(200).send({
            token: token,
            name: user.firstname
        });
    }

    return {
        passwordHash: passwordHash,
        comparePasswords: comparePasswords,
        createToken: createToken

    };
};