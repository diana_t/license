/**
 * Created by dianaturnea on 10/11/16.
 */

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var MongoClient = require('mongodb').MongoClient;

var mainModule = require('./server/modules/main')();
var loginModule = require('./server/modules/login');
var registerModule = require('./server/modules/register');
var accountModule = require('./server/modules/account');
var componentsModule = require('./server/modules/components');

var envConfig = require('./env.json')[process.env.NODE_ENV || 'development'];
var url = 'mongodb://localhost:27017/license'; //TODO
var port = envConfig.PORT || 8000;

var passport = require('passport');

app.use(express.static('client/public'));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cookieParser());
app.use(session({
    secret: 'license',
    name: 'license',
    proxy: true,
    resave: true,
    saveUninitialized: true
}));

app.set('views', __dirname + '/client/views');
app.set('view engine', '.ejs');
app.engine('.html', require('ejs').__express);
app.engine('ejs', require('ejs').renderFile);

MongoClient.connect(url, function(err, db) {

    //Modules
    app.use('/', mainModule);
    app.use('/login', loginModule(db));
    app.use('/register', registerModule(db));
    app.use('/account', accountModule(db));
    app.use('/components', componentsModule());

    app.listen(port, function(err) {
        if (err) {
            console.log('Error...');
        }
        console.log('Running server on port ' + port);
    });

});