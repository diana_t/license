'use strict';
var gulp = require('gulp'),
    nodemon = require('nodemon'),
    jshint = require('gulp-jshint'),
    jscs = require('gulp-jscs');

var jsFiles = ['client/public/js/*.js', 'server/**/*.js', 'server/*.js'];

// "gulp start" - start server
gulp.task('start', ['validate-code'], function () {
    var options = {
        script: 'server/server.js',
        delayTime: 1,
        watch: jsFiles,
        ext: 'js'
    };

    return nodemon(options)
        .on('restart', function () {
            console.log('Restarting....');
        });
});

// "gulp validate-code" Command use for check that the code is clean
gulp.task('validate-code', function () {
    return gulp.src(jsFiles)
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish', {
            verbose: true
        }))
        .pipe(jscs());
});
// "gulp-inject" - command use for inject the css and js files in html pages
gulp.task('inject', function () {
    var wiredep = require('wiredep').stream,
        inject = require('gulp-inject');

    var injectSrc = gulp.src(['./client/public/css/*.css', './client/public/js/*.js','./client/public/js/**/*.js'], {
        read: false
    });
    var injectOptions = {
        ignorePath: '/client/public'
    };
    var options = {
        bowerJson: require('./bower.json'),
        directory: 'client/public/lib',
        ignorePath: '../../public'
    };
    
    gulp.task('inject', function () {
        return gulp.src('./client/views/**/*.ejs')
            .pipe(wiredep(options))
            .pipe(inject(injectSrc, injectOptions, {
                read: false
            }), {
                name: 'bower',
                relative: true
            })
            .pipe(gulp.dest('./client/views'));
    });

});