License 2017 - Diana Turnea

Add components in lib
#bower install#

next -> gulp (Task manager for web projects)

JSHint  - Code quality enforcement (https://github.com/jshint/jshint) - http://jshint.com/
            1.detects potential errors
            2.enforces coding conventions
            3.easily configurable
            4.open source
JSCS - Code style enforcement (https://github.com/jscs-dev/node-jscs) - http://jscs.info/
            1.enforces style conventions
            2.easily configurable
            3.open source
            
link(https://github.com/jonathanfmills/CodingStandards)